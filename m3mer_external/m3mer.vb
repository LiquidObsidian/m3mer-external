﻿Imports System.Windows.Forms
Imports System.IO

Public Class m3mer
    Private Sub CloseApp(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Environment.Exit(0)
    End Sub

    Private Sub TitlePicture_Click(sender As Object, e As EventArgs) Handles TitlePicture.Click
        System.Diagnostics.Process.Start("http://liqob.com/m3mers/")
    End Sub

    Private Sub m3mer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Config.Init()
    End Sub

    Private Sub updateTimer_Tick(sender As Object, e As EventArgs) Handles updateTimer.Tick
        Config.UpdateConfig()
    End Sub

    Private Sub Save_Click(sender As Object, e As EventArgs) Handles Save.Click
        Config.Save()
    End Sub

    Private Sub KeycodesLabel_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles KeycodesLabel.LinkClicked
        System.Diagnostics.Process.Start("http://liqob.com/m3mers/keycodes")
    End Sub
End Class