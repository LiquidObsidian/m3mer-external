﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class m3mer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(m3mer))
        Me.TitlePicture = New System.Windows.Forms.PictureBox()
        Me.OptionsBox = New System.Windows.Forms.GroupBox()
        Me.AimSmooth = New System.Windows.Forms.TextBox()
        Me.AimbotSmoothnessLABEL = New System.Windows.Forms.Label()
        Me.AimbotFOV = New System.Windows.Forms.TextBox()
        Me.AimbotFOVLabel = New System.Windows.Forms.Label()
        Me.AimbotBone = New System.Windows.Forms.TextBox()
        Me.AimbotBoneLabel = New System.Windows.Forms.Label()
        Me.GlowAlphaLabel = New System.Windows.Forms.Label()
        Me.GlowAlpha = New System.Windows.Forms.TextBox()
        Me.KeycodesLabel = New System.Windows.Forms.LinkLabel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.EnemyLabel = New System.Windows.Forms.Label()
        Me.TeamLabel = New System.Windows.Forms.Label()
        Me.Sensitivity = New System.Windows.Forms.TextBox()
        Me.AutoPistolKey = New System.Windows.Forms.TextBox()
        Me.SlowAimSensitivity = New System.Windows.Forms.TextBox()
        Me.EnemyBlue = New System.Windows.Forms.TextBox()
        Me.TriggerBotKey = New System.Windows.Forms.TextBox()
        Me.TriggerBotDelay = New System.Windows.Forms.TextBox()
        Me.EnemyGreen = New System.Windows.Forms.TextBox()
        Me.TeamBlue = New System.Windows.Forms.TextBox()
        Me.EnemyRed = New System.Windows.Forms.TextBox()
        Me.TeamGreen = New System.Windows.Forms.TextBox()
        Me.TeamRed = New System.Windows.Forms.TextBox()
        Me.SlowAimLabel = New System.Windows.Forms.Label()
        Me.SensitivityLabel = New System.Windows.Forms.Label()
        Me.AutoPistolKeyLabel = New System.Windows.Forms.Label()
        Me.TriggerBotDelayLabel = New System.Windows.Forms.Label()
        Me.TriggerBotKeyLabel = New System.Windows.Forms.Label()
        Me.FeaturesBox = New System.Windows.Forms.GroupBox()
        Me.EnemyRainbow = New System.Windows.Forms.CheckBox()
        Me.TeamRainbow = New System.Windows.Forms.CheckBox()
        Me.Aimbot = New System.Windows.Forms.CheckBox()
        Me.NoFlash = New System.Windows.Forms.CheckBox()
        Me.Triggerbot = New System.Windows.Forms.CheckBox()
        Me.GlowEnemy = New System.Windows.Forms.CheckBox()
        Me.GlowTeam = New System.Windows.Forms.CheckBox()
        Me.AutoPistol = New System.Windows.Forms.CheckBox()
        Me.SlowAim = New System.Windows.Forms.CheckBox()
        Me.BunnyHop = New System.Windows.Forms.CheckBox()
        Me.Radar = New System.Windows.Forms.CheckBox()
        Me.updateTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Save = New System.Windows.Forms.Button()
        CType(Me.TitlePicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OptionsBox.SuspendLayout()
        Me.FeaturesBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'TitlePicture
        '
        Me.TitlePicture.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TitlePicture.Image = Global.m3mer_external.My.Resources.Resources.m3merpublic
        Me.TitlePicture.Location = New System.Drawing.Point(12, 6)
        Me.TitlePicture.Name = "TitlePicture"
        Me.TitlePicture.Size = New System.Drawing.Size(205, 60)
        Me.TitlePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.TitlePicture.TabIndex = 0
        Me.TitlePicture.TabStop = False
        '
        'OptionsBox
        '
        Me.OptionsBox.Controls.Add(Me.AimSmooth)
        Me.OptionsBox.Controls.Add(Me.AimbotSmoothnessLABEL)
        Me.OptionsBox.Controls.Add(Me.AimbotFOV)
        Me.OptionsBox.Controls.Add(Me.AimbotFOVLabel)
        Me.OptionsBox.Controls.Add(Me.AimbotBone)
        Me.OptionsBox.Controls.Add(Me.AimbotBoneLabel)
        Me.OptionsBox.Controls.Add(Me.GlowAlphaLabel)
        Me.OptionsBox.Controls.Add(Me.GlowAlpha)
        Me.OptionsBox.Controls.Add(Me.KeycodesLabel)
        Me.OptionsBox.Controls.Add(Me.Label5)
        Me.OptionsBox.Controls.Add(Me.Label4)
        Me.OptionsBox.Controls.Add(Me.Label3)
        Me.OptionsBox.Controls.Add(Me.EnemyLabel)
        Me.OptionsBox.Controls.Add(Me.TeamLabel)
        Me.OptionsBox.Controls.Add(Me.Sensitivity)
        Me.OptionsBox.Controls.Add(Me.AutoPistolKey)
        Me.OptionsBox.Controls.Add(Me.SlowAimSensitivity)
        Me.OptionsBox.Controls.Add(Me.EnemyBlue)
        Me.OptionsBox.Controls.Add(Me.TriggerBotKey)
        Me.OptionsBox.Controls.Add(Me.TriggerBotDelay)
        Me.OptionsBox.Controls.Add(Me.EnemyGreen)
        Me.OptionsBox.Controls.Add(Me.TeamBlue)
        Me.OptionsBox.Controls.Add(Me.EnemyRed)
        Me.OptionsBox.Controls.Add(Me.TeamGreen)
        Me.OptionsBox.Controls.Add(Me.TeamRed)
        Me.OptionsBox.Controls.Add(Me.SlowAimLabel)
        Me.OptionsBox.Controls.Add(Me.SensitivityLabel)
        Me.OptionsBox.Controls.Add(Me.AutoPistolKeyLabel)
        Me.OptionsBox.Controls.Add(Me.TriggerBotDelayLabel)
        Me.OptionsBox.Controls.Add(Me.TriggerBotKeyLabel)
        Me.OptionsBox.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OptionsBox.ForeColor = System.Drawing.SystemColors.Control
        Me.OptionsBox.Location = New System.Drawing.Point(223, 39)
        Me.OptionsBox.Name = "OptionsBox"
        Me.OptionsBox.Size = New System.Drawing.Size(244, 249)
        Me.OptionsBox.TabIndex = 1
        Me.OptionsBox.TabStop = False
        Me.OptionsBox.Text = "Options -"
        '
        'AimSmooth
        '
        Me.AimSmooth.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.AimSmooth.Location = New System.Drawing.Point(192, 136)
        Me.AimSmooth.Name = "AimSmooth"
        Me.AimSmooth.Size = New System.Drawing.Size(30, 16)
        Me.AimSmooth.TabIndex = 25
        Me.AimSmooth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AimbotSmoothnessLABEL
        '
        Me.AimbotSmoothnessLABEL.AutoSize = True
        Me.AimbotSmoothnessLABEL.Location = New System.Drawing.Point(8, 136)
        Me.AimbotSmoothnessLABEL.Name = "AimbotSmoothnessLABEL"
        Me.AimbotSmoothnessLABEL.Size = New System.Drawing.Size(138, 16)
        Me.AimbotSmoothnessLABEL.TabIndex = 24
        Me.AimbotSmoothnessLABEL.Text = "Aimbot Smoothness"
        '
        'AimbotFOV
        '
        Me.AimbotFOV.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.AimbotFOV.Location = New System.Drawing.Point(192, 120)
        Me.AimbotFOV.Name = "AimbotFOV"
        Me.AimbotFOV.Size = New System.Drawing.Size(30, 16)
        Me.AimbotFOV.TabIndex = 23
        Me.AimbotFOV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AimbotFOVLabel
        '
        Me.AimbotFOVLabel.AutoSize = True
        Me.AimbotFOVLabel.Location = New System.Drawing.Point(8, 120)
        Me.AimbotFOVLabel.Name = "AimbotFOVLabel"
        Me.AimbotFOVLabel.Size = New System.Drawing.Size(85, 16)
        Me.AimbotFOVLabel.TabIndex = 22
        Me.AimbotFOVLabel.Text = "Aimbot FOV"
        '
        'AimbotBone
        '
        Me.AimbotBone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.AimbotBone.Location = New System.Drawing.Point(192, 104)
        Me.AimbotBone.Name = "AimbotBone"
        Me.AimbotBone.Size = New System.Drawing.Size(30, 16)
        Me.AimbotBone.TabIndex = 21
        Me.AimbotBone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AimbotBoneLabel
        '
        Me.AimbotBoneLabel.AutoSize = True
        Me.AimbotBoneLabel.Location = New System.Drawing.Point(8, 104)
        Me.AimbotBoneLabel.Name = "AimbotBoneLabel"
        Me.AimbotBoneLabel.Size = New System.Drawing.Size(90, 16)
        Me.AimbotBoneLabel.TabIndex = 20
        Me.AimbotBoneLabel.Text = "Aimbot Bone"
        '
        'GlowAlphaLabel
        '
        Me.GlowAlphaLabel.AutoSize = True
        Me.GlowAlphaLabel.Location = New System.Drawing.Point(32, 224)
        Me.GlowAlphaLabel.Name = "GlowAlphaLabel"
        Me.GlowAlphaLabel.Size = New System.Drawing.Size(44, 16)
        Me.GlowAlphaLabel.TabIndex = 19
        Me.GlowAlphaLabel.Text = "Alpha"
        '
        'GlowAlpha
        '
        Me.GlowAlpha.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GlowAlpha.Location = New System.Drawing.Point(88, 224)
        Me.GlowAlpha.Name = "GlowAlpha"
        Me.GlowAlpha.Size = New System.Drawing.Size(30, 16)
        Me.GlowAlpha.TabIndex = 18
        Me.GlowAlpha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'KeycodesLabel
        '
        Me.KeycodesLabel.AutoSize = True
        Me.KeycodesLabel.LinkColor = System.Drawing.Color.DarkTurquoise
        Me.KeycodesLabel.Location = New System.Drawing.Point(72, 0)
        Me.KeycodesLabel.Name = "KeycodesLabel"
        Me.KeycodesLabel.Size = New System.Drawing.Size(71, 16)
        Me.KeycodesLabel.TabIndex = 7
        Me.KeycodesLabel.TabStop = True
        Me.KeycodesLabel.Text = "Keycodes"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(40, 208)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 16)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Blue"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 192)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 16)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Green"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(48, 176)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Red"
        '
        'EnemyLabel
        '
        Me.EnemyLabel.AutoSize = True
        Me.EnemyLabel.BackColor = System.Drawing.Color.Transparent
        Me.EnemyLabel.Font = New System.Drawing.Font("Verdana", 6.5!)
        Me.EnemyLabel.Location = New System.Drawing.Point(120, 160)
        Me.EnemyLabel.Name = "EnemyLabel"
        Me.EnemyLabel.Size = New System.Drawing.Size(39, 12)
        Me.EnemyLabel.TabIndex = 12
        Me.EnemyLabel.Text = "Enemy"
        '
        'TeamLabel
        '
        Me.TeamLabel.AutoSize = True
        Me.TeamLabel.BackColor = System.Drawing.Color.Transparent
        Me.TeamLabel.Font = New System.Drawing.Font("Verdana", 6.5!)
        Me.TeamLabel.Location = New System.Drawing.Point(88, 160)
        Me.TeamLabel.Name = "TeamLabel"
        Me.TeamLabel.Size = New System.Drawing.Size(33, 12)
        Me.TeamLabel.TabIndex = 12
        Me.TeamLabel.Text = "Team"
        '
        'Sensitivity
        '
        Me.Sensitivity.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Sensitivity.Location = New System.Drawing.Point(192, 72)
        Me.Sensitivity.Name = "Sensitivity"
        Me.Sensitivity.Size = New System.Drawing.Size(30, 16)
        Me.Sensitivity.TabIndex = 10
        Me.Sensitivity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AutoPistolKey
        '
        Me.AutoPistolKey.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.AutoPistolKey.CausesValidation = False
        Me.AutoPistolKey.Location = New System.Drawing.Point(192, 56)
        Me.AutoPistolKey.Name = "AutoPistolKey"
        Me.AutoPistolKey.Size = New System.Drawing.Size(30, 16)
        Me.AutoPistolKey.TabIndex = 9
        Me.AutoPistolKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SlowAimSensitivity
        '
        Me.SlowAimSensitivity.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SlowAimSensitivity.Location = New System.Drawing.Point(192, 88)
        Me.SlowAimSensitivity.Name = "SlowAimSensitivity"
        Me.SlowAimSensitivity.Size = New System.Drawing.Size(30, 16)
        Me.SlowAimSensitivity.TabIndex = 11
        Me.SlowAimSensitivity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EnemyBlue
        '
        Me.EnemyBlue.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.EnemyBlue.Location = New System.Drawing.Point(120, 208)
        Me.EnemyBlue.Name = "EnemyBlue"
        Me.EnemyBlue.Size = New System.Drawing.Size(30, 16)
        Me.EnemyBlue.TabIndex = 17
        Me.EnemyBlue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TriggerBotKey
        '
        Me.TriggerBotKey.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TriggerBotKey.Location = New System.Drawing.Point(192, 24)
        Me.TriggerBotKey.Name = "TriggerBotKey"
        Me.TriggerBotKey.Size = New System.Drawing.Size(30, 16)
        Me.TriggerBotKey.TabIndex = 8
        Me.TriggerBotKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TriggerBotDelay
        '
        Me.TriggerBotDelay.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TriggerBotDelay.Location = New System.Drawing.Point(192, 40)
        Me.TriggerBotDelay.Name = "TriggerBotDelay"
        Me.TriggerBotDelay.Size = New System.Drawing.Size(30, 16)
        Me.TriggerBotDelay.TabIndex = 8
        Me.TriggerBotDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EnemyGreen
        '
        Me.EnemyGreen.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.EnemyGreen.Location = New System.Drawing.Point(120, 192)
        Me.EnemyGreen.Name = "EnemyGreen"
        Me.EnemyGreen.Size = New System.Drawing.Size(30, 16)
        Me.EnemyGreen.TabIndex = 16
        Me.EnemyGreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TeamBlue
        '
        Me.TeamBlue.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TeamBlue.Location = New System.Drawing.Point(88, 208)
        Me.TeamBlue.Name = "TeamBlue"
        Me.TeamBlue.Size = New System.Drawing.Size(30, 16)
        Me.TeamBlue.TabIndex = 14
        Me.TeamBlue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EnemyRed
        '
        Me.EnemyRed.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.EnemyRed.Location = New System.Drawing.Point(120, 176)
        Me.EnemyRed.Name = "EnemyRed"
        Me.EnemyRed.Size = New System.Drawing.Size(30, 16)
        Me.EnemyRed.TabIndex = 15
        Me.EnemyRed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TeamGreen
        '
        Me.TeamGreen.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TeamGreen.Location = New System.Drawing.Point(88, 192)
        Me.TeamGreen.Name = "TeamGreen"
        Me.TeamGreen.Size = New System.Drawing.Size(30, 16)
        Me.TeamGreen.TabIndex = 13
        Me.TeamGreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TeamRed
        '
        Me.TeamRed.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TeamRed.Location = New System.Drawing.Point(88, 176)
        Me.TeamRed.Name = "TeamRed"
        Me.TeamRed.Size = New System.Drawing.Size(30, 16)
        Me.TeamRed.TabIndex = 12
        Me.TeamRed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SlowAimLabel
        '
        Me.SlowAimLabel.AutoSize = True
        Me.SlowAimLabel.Location = New System.Drawing.Point(8, 88)
        Me.SlowAimLabel.Name = "SlowAimLabel"
        Me.SlowAimLabel.Size = New System.Drawing.Size(67, 16)
        Me.SlowAimLabel.TabIndex = 0
        Me.SlowAimLabel.Text = "Slow Aim"
        '
        'SensitivityLabel
        '
        Me.SensitivityLabel.AutoSize = True
        Me.SensitivityLabel.Location = New System.Drawing.Point(8, 72)
        Me.SensitivityLabel.Name = "SensitivityLabel"
        Me.SensitivityLabel.Size = New System.Drawing.Size(77, 16)
        Me.SensitivityLabel.TabIndex = 0
        Me.SensitivityLabel.Text = "Sensitivity"
        '
        'AutoPistolKeyLabel
        '
        Me.AutoPistolKeyLabel.AutoSize = True
        Me.AutoPistolKeyLabel.Location = New System.Drawing.Point(8, 56)
        Me.AutoPistolKeyLabel.Name = "AutoPistolKeyLabel"
        Me.AutoPistolKeyLabel.Size = New System.Drawing.Size(108, 16)
        Me.AutoPistolKeyLabel.TabIndex = 0
        Me.AutoPistolKeyLabel.Text = "Auto Pistol Key"
        '
        'TriggerBotDelayLabel
        '
        Me.TriggerBotDelayLabel.AutoSize = True
        Me.TriggerBotDelayLabel.Location = New System.Drawing.Point(8, 40)
        Me.TriggerBotDelayLabel.Name = "TriggerBotDelayLabel"
        Me.TriggerBotDelayLabel.Size = New System.Drawing.Size(117, 16)
        Me.TriggerBotDelayLabel.TabIndex = 0
        Me.TriggerBotDelayLabel.Text = "Triggerbot Delay"
        '
        'TriggerBotKeyLabel
        '
        Me.TriggerBotKeyLabel.AutoSize = True
        Me.TriggerBotKeyLabel.Location = New System.Drawing.Point(8, 24)
        Me.TriggerBotKeyLabel.Name = "TriggerBotKeyLabel"
        Me.TriggerBotKeyLabel.Size = New System.Drawing.Size(105, 16)
        Me.TriggerBotKeyLabel.TabIndex = 0
        Me.TriggerBotKeyLabel.Text = "Triggerbot Key"
        '
        'FeaturesBox
        '
        Me.FeaturesBox.Controls.Add(Me.EnemyRainbow)
        Me.FeaturesBox.Controls.Add(Me.TeamRainbow)
        Me.FeaturesBox.Controls.Add(Me.Aimbot)
        Me.FeaturesBox.Controls.Add(Me.NoFlash)
        Me.FeaturesBox.Controls.Add(Me.Triggerbot)
        Me.FeaturesBox.Controls.Add(Me.GlowEnemy)
        Me.FeaturesBox.Controls.Add(Me.GlowTeam)
        Me.FeaturesBox.Controls.Add(Me.AutoPistol)
        Me.FeaturesBox.Controls.Add(Me.SlowAim)
        Me.FeaturesBox.Controls.Add(Me.BunnyHop)
        Me.FeaturesBox.Controls.Add(Me.Radar)
        Me.FeaturesBox.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FeaturesBox.ForeColor = System.Drawing.SystemColors.Control
        Me.FeaturesBox.Location = New System.Drawing.Point(12, 72)
        Me.FeaturesBox.Name = "FeaturesBox"
        Me.FeaturesBox.Size = New System.Drawing.Size(205, 216)
        Me.FeaturesBox.TabIndex = 2
        Me.FeaturesBox.TabStop = False
        Me.FeaturesBox.Text = "Features"
        '
        'EnemyRainbow
        '
        Me.EnemyRainbow.AutoSize = True
        Me.EnemyRainbow.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.EnemyRainbow.Location = New System.Drawing.Point(8, 162)
        Me.EnemyRainbow.Name = "EnemyRainbow"
        Me.EnemyRainbow.Size = New System.Drawing.Size(127, 20)
        Me.EnemyRainbow.TabIndex = 10
        Me.EnemyRainbow.Text = "Enemy Rainbow"
        Me.EnemyRainbow.UseVisualStyleBackColor = True
        '
        'TeamRainbow
        '
        Me.TeamRainbow.AutoSize = True
        Me.TeamRainbow.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.TeamRainbow.Location = New System.Drawing.Point(8, 145)
        Me.TeamRainbow.Name = "TeamRainbow"
        Me.TeamRainbow.Size = New System.Drawing.Size(120, 20)
        Me.TeamRainbow.TabIndex = 9
        Me.TeamRainbow.Text = "Team Rainbow"
        Me.TeamRainbow.UseVisualStyleBackColor = True
        '
        'Aimbot
        '
        Me.Aimbot.AutoSize = True
        Me.Aimbot.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Aimbot.Location = New System.Drawing.Point(8, 16)
        Me.Aimbot.Name = "Aimbot"
        Me.Aimbot.Size = New System.Drawing.Size(70, 20)
        Me.Aimbot.TabIndex = 8
        Me.Aimbot.Text = "Aimbot"
        Me.Aimbot.UseVisualStyleBackColor = True
        '
        'NoFlash
        '
        Me.NoFlash.AutoSize = True
        Me.NoFlash.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.NoFlash.Location = New System.Drawing.Point(8, 179)
        Me.NoFlash.Name = "NoFlash"
        Me.NoFlash.Size = New System.Drawing.Size(81, 20)
        Me.NoFlash.TabIndex = 7
        Me.NoFlash.Text = "No Flash"
        Me.NoFlash.UseVisualStyleBackColor = True
        '
        'Triggerbot
        '
        Me.Triggerbot.AutoSize = True
        Me.Triggerbot.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Triggerbot.Location = New System.Drawing.Point(8, 64)
        Me.Triggerbot.Name = "Triggerbot"
        Me.Triggerbot.Size = New System.Drawing.Size(93, 20)
        Me.Triggerbot.TabIndex = 0
        Me.Triggerbot.Text = "Triggerbot"
        Me.Triggerbot.UseVisualStyleBackColor = True
        '
        'GlowEnemy
        '
        Me.GlowEnemy.AutoSize = True
        Me.GlowEnemy.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GlowEnemy.Location = New System.Drawing.Point(8, 128)
        Me.GlowEnemy.Name = "GlowEnemy"
        Me.GlowEnemy.Size = New System.Drawing.Size(104, 20)
        Me.GlowEnemy.TabIndex = 6
        Me.GlowEnemy.Text = "Glow Enemy"
        Me.GlowEnemy.UseVisualStyleBackColor = True
        '
        'GlowTeam
        '
        Me.GlowTeam.AutoSize = True
        Me.GlowTeam.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GlowTeam.Location = New System.Drawing.Point(8, 112)
        Me.GlowTeam.Name = "GlowTeam"
        Me.GlowTeam.Size = New System.Drawing.Size(97, 20)
        Me.GlowTeam.TabIndex = 5
        Me.GlowTeam.Text = "Glow Team"
        Me.GlowTeam.UseVisualStyleBackColor = True
        '
        'AutoPistol
        '
        Me.AutoPistol.AutoSize = True
        Me.AutoPistol.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AutoPistol.Location = New System.Drawing.Point(8, 96)
        Me.AutoPistol.Name = "AutoPistol"
        Me.AutoPistol.Size = New System.Drawing.Size(96, 20)
        Me.AutoPistol.TabIndex = 4
        Me.AutoPistol.Text = "Auto Pistol"
        Me.AutoPistol.UseVisualStyleBackColor = True
        '
        'SlowAim
        '
        Me.SlowAim.AutoSize = True
        Me.SlowAim.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SlowAim.Location = New System.Drawing.Point(8, 80)
        Me.SlowAim.Name = "SlowAim"
        Me.SlowAim.Size = New System.Drawing.Size(84, 20)
        Me.SlowAim.TabIndex = 3
        Me.SlowAim.Text = "Slow Aim"
        Me.SlowAim.UseVisualStyleBackColor = True
        '
        'BunnyHop
        '
        Me.BunnyHop.AutoSize = True
        Me.BunnyHop.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BunnyHop.Location = New System.Drawing.Point(8, 48)
        Me.BunnyHop.Name = "BunnyHop"
        Me.BunnyHop.Size = New System.Drawing.Size(95, 20)
        Me.BunnyHop.TabIndex = 2
        Me.BunnyHop.Text = "Bunny Hop"
        Me.BunnyHop.UseVisualStyleBackColor = True
        '
        'Radar
        '
        Me.Radar.AutoSize = True
        Me.Radar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Radar.Location = New System.Drawing.Point(8, 32)
        Me.Radar.Name = "Radar"
        Me.Radar.Size = New System.Drawing.Size(62, 20)
        Me.Radar.TabIndex = 1
        Me.Radar.Text = "Radar"
        Me.Radar.UseVisualStyleBackColor = True
        '
        'updateTimer
        '
        Me.updateTimer.Enabled = True
        '
        'Save
        '
        Me.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Save.ForeColor = System.Drawing.SystemColors.Control
        Me.Save.Location = New System.Drawing.Point(223, 6)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(244, 27)
        Me.Save.TabIndex = 18
        Me.Save.Text = "Save"
        Me.Save.UseVisualStyleBackColor = True
        '
        'm3mer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.RoyalBlue
        Me.ClientSize = New System.Drawing.Size(479, 297)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.FeaturesBox)
        Me.Controls.Add(Me.OptionsBox)
        Me.Controls.Add(Me.TitlePicture)
        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "m3mer"
        Me.Opacity = 0.9R
        CType(Me.TitlePicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OptionsBox.ResumeLayout(False)
        Me.OptionsBox.PerformLayout()
        Me.FeaturesBox.ResumeLayout(False)
        Me.FeaturesBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TitlePicture As System.Windows.Forms.PictureBox
    Friend WithEvents OptionsBox As System.Windows.Forms.GroupBox
    Friend WithEvents FeaturesBox As System.Windows.Forms.GroupBox
    Friend WithEvents GlowTeam As System.Windows.Forms.CheckBox
    Friend WithEvents AutoPistol As System.Windows.Forms.CheckBox
    Friend WithEvents SlowAim As System.Windows.Forms.CheckBox
    Friend WithEvents BunnyHop As System.Windows.Forms.CheckBox
    Friend WithEvents Radar As System.Windows.Forms.CheckBox
    Friend WithEvents Triggerbot As System.Windows.Forms.CheckBox
    Friend WithEvents GlowEnemy As System.Windows.Forms.CheckBox
    Friend WithEvents SlowAimLabel As System.Windows.Forms.Label
    Friend WithEvents SensitivityLabel As System.Windows.Forms.Label
    Friend WithEvents AutoPistolKeyLabel As System.Windows.Forms.Label
    Friend WithEvents TriggerBotDelayLabel As System.Windows.Forms.Label
    Friend WithEvents TriggerBotKeyLabel As System.Windows.Forms.Label
    Friend WithEvents TeamRed As System.Windows.Forms.TextBox
    Friend WithEvents Sensitivity As System.Windows.Forms.TextBox
    Friend WithEvents AutoPistolKey As System.Windows.Forms.TextBox
    Friend WithEvents SlowAimSensitivity As System.Windows.Forms.TextBox
    Friend WithEvents TriggerBotDelay As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents EnemyLabel As System.Windows.Forms.Label
    Friend WithEvents TeamLabel As System.Windows.Forms.Label
    Friend WithEvents EnemyBlue As System.Windows.Forms.TextBox
    Friend WithEvents EnemyGreen As System.Windows.Forms.TextBox
    Friend WithEvents TeamBlue As System.Windows.Forms.TextBox
    Friend WithEvents EnemyRed As System.Windows.Forms.TextBox
    Friend WithEvents TeamGreen As System.Windows.Forms.TextBox
    Friend WithEvents TriggerBotKey As System.Windows.Forms.TextBox
    Friend WithEvents updateTimer As System.Windows.Forms.Timer
    Friend WithEvents Save As System.Windows.Forms.Button
    Friend WithEvents KeycodesLabel As System.Windows.Forms.LinkLabel
    Friend WithEvents GlowAlpha As System.Windows.Forms.TextBox
    Friend WithEvents GlowAlphaLabel As System.Windows.Forms.Label
    Friend WithEvents NoFlash As System.Windows.Forms.CheckBox
    Friend WithEvents Aimbot As System.Windows.Forms.CheckBox
    Friend WithEvents AimbotBoneLabel As System.Windows.Forms.Label
    Friend WithEvents AimbotBone As System.Windows.Forms.TextBox
    Friend WithEvents EnemyRainbow As CheckBox
    Friend WithEvents TeamRainbow As CheckBox
    Friend WithEvents AimbotFOVLabel As System.Windows.Forms.Label
    Friend WithEvents AimbotFOV As System.Windows.Forms.TextBox
    Friend WithEvents AimbotSmoothnessLABEL As System.Windows.Forms.Label
    Friend WithEvents AimSmooth As System.Windows.Forms.TextBox
End Class
