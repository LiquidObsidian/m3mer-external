﻿Imports System.Windows.Forms
Imports System.IO

Module Config
    ' DEFAULT CONFIG
    Public key_trigger As Integer = Keys.XButton2
    Public key_autopistol As Integer = Keys.XButton1
    Public delay_trigger As Integer = 5
    Public sensitivity_old As Single = 1.25F
    Public sensitivity_slowaim As Single = 0.5F
    Public glow_enemy_b As Integer = 0
    Public glow_team_r As Integer = 0
    Public glow_team_g As Integer = 0
    Public glow_team_b As Integer = 255
    Public glow_alpha As Integer = 175
    Public aim_bone As Integer = 6
    Public aim_fov As Integer = 8
    Public aim_smooth As Integer = 6
    Public glow_enemy_r As Integer = 255
    Public glow_enemy_g As Integer = 0
    ' END DEFAULT CONFIG

    Public Sub Init()
        Load()
        UpdateMenu()
        Save()
    End Sub

    Public Sub UpdateConfig()
        Try
            key_trigger = Convert.ToInt32(menu.TriggerBotKey.Text)
            key_autopistol = Convert.ToInt32(menu.AutoPistolKey.Text)
            delay_trigger = Convert.ToInt32(menu.TriggerBotDelay.Text)
            glow_enemy_g = Convert.ToInt32(menu.EnemyGreen.Text)
            glow_enemy_b = Convert.ToInt32(menu.EnemyBlue.Text)
            glow_team_r = Convert.ToInt32(menu.TeamRed.Text)
            glow_team_g = Convert.ToInt32(menu.TeamGreen.Text)
            glow_team_b = Convert.ToInt32(menu.TeamBlue.Text)
            glow_alpha = Convert.ToInt32(menu.GlowAlpha.Text)
            aim_bone = Convert.ToInt32(menu.AimbotBone.Text)
            aim_fov = Convert.ToInt32(menu.AimbotFOV.Text)
            aim_smooth = Convert.ToInt32(menu.AimSmooth.Text)
            sensitivity_old = Convert.ToSingle(menu.Sensitivity.Text)
            sensitivity_slowaim = Convert.ToSingle(menu.SlowAimSensitivity.Text)
            glow_enemy_r = Convert.ToInt32(menu.EnemyRed.Text)
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
        End Try
    End Sub

    Public Sub UpdateMenu()
        menu.TriggerBotKey.Text = key_trigger
        menu.TriggerBotDelay.Text = delay_trigger
        menu.AutoPistolKey.Text = key_autopistol
        menu.EnemyRed.Text = glow_enemy_r
        menu.EnemyGreen.Text = glow_enemy_g
        menu.EnemyBlue.Text = glow_enemy_b
        menu.TeamRed.Text = glow_team_r
        menu.TeamGreen.Text = glow_team_g
        menu.TeamBlue.Text = glow_team_b
        menu.GlowAlpha.Text = glow_alpha
        menu.AimbotBone.Text = aim_bone
        menu.AimbotFOV.Text = aim_fov
        menu.AimSmooth.Text = aim_smooth
        menu.Sensitivity.Text = sensitivity_old
        menu.SlowAimSensitivity.Text = sensitivity_slowaim
    End Sub

    Public Sub Save()
        Try
            INI.write(Application.StartupPath + "/settings.ini", "settings", "slowaim_sensitivity", menu.SlowAimSensitivity.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "autopistol_enabled", menu.AutoPistol.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "autopistol_key", menu.AutoPistolKey.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_enabled", menu.GlowTeam.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_enabled", menu.GlowEnemy.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_rainbow", menu.TeamRainbow.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_rainbow", menu.EnemyRainbow.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_r", menu.TeamRed.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_g", menu.TeamGreen.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_b", menu.TeamBlue.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_r", menu.EnemyRed.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "triggerbot_enabled", menu.Triggerbot.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "triggerbot_key", Convert.ToInt32(menu.TriggerBotKey.Text))
            INI.write(Application.StartupPath + "/settings.ini", "settings", "triggerbot_delay", Convert.ToInt32(menu.TriggerBotDelay.Text))
            INI.write(Application.StartupPath + "/settings.ini", "settings", "radar_enabled", menu.Radar.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "bunnyhop_enabled", menu.BunnyHop.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "slowaim_enabled", menu.SlowAim.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "slowaim_oldsensitivity", menu.Sensitivity.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_alpha", menu.GlowAlpha.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "noflash_enabled", menu.NoFlash.Checked)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "aim_bone", menu.AimbotBone.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "aim_fov", menu.AimbotFOV.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "aim_smooth", menu.AimSmooth.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_g", menu.EnemyGreen.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_b", menu.EnemyBlue.Text)
            INI.write(Application.StartupPath + "/settings.ini", "settings", "aimbot_enabled", menu.Aimbot.Checked)
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
        End Try
    End Sub

    Public Sub Load()
        If File.Exists(Application.StartupPath + "/settings.ini") Then
            Try
                sensitivity_old = Convert.ToSingle(INI.read(Application.StartupPath + "/settings.ini", "settings", "slowaim_oldsensitivity"))
                sensitivity_slowaim = Convert.ToSingle(INI.read(Application.StartupPath + "/settings.ini", "settings", "slowaim_sensitivity"))
                menu.AutoPistol.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "autopistol_enabled")
                key_autopistol = INI.read(Application.StartupPath + "/settings.ini", "settings", "autopistol_key")
                menu.GlowTeam.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_enabled")
                menu.GlowEnemy.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_enabled")
                menu.TeamRainbow.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_rainbow")
                menu.EnemyRainbow.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_rainbow")
                glow_team_r = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_r")
                glow_enemy_b = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_b")
                glow_alpha = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_alpha")
                menu.NoFlash.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "noflash_enabled")
                menu.Aimbot.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "aimbot_enabled")
                menu.Triggerbot.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "triggerbot_enabled")
                key_trigger = INI.read(Application.StartupPath + "/settings.ini", "settings", "triggerbot_key")
                delay_trigger = INI.read(Application.StartupPath + "/settings.ini", "settings", "triggerbot_delay")
                menu.Radar.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "radar_enabled")
                menu.BunnyHop.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "bunnyhop_enabled")
                glow_team_g = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_g")
                glow_team_b = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_team_b")
                glow_enemy_r = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_r")
                glow_enemy_g = INI.read(Application.StartupPath + "/settings.ini", "settings", "glowesp_enemy_g")
                menu.SlowAim.Checked = INI.read(Application.StartupPath + "/settings.ini", "settings", "slowaim_enabled")
                aim_bone = INI.read(Application.StartupPath + "/settings.ini", "settings", "aim_bone")
                aim_fov = INI.read(Application.StartupPath + "/settings.ini", "settings", "aim_fov")
                aim_smooth = INI.read(Application.StartupPath + "/settings.ini", "settings", "aim_smooth")
            Catch ex As Exception
                Console.WriteLine(ex.ToString())
            End Try
        End If
    End Sub
End Module