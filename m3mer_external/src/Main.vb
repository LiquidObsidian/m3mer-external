﻿Imports System
Imports System.Windows.Forms
Imports System.IO
Imports System.Text

Public Module Main
    Public Declare Function ReadProcessMemory Lib "kernel32" (ByVal hProcess As IntPtr, ByVal lpBaseAddress As IntPtr, ByVal lpBuffer() As Byte, ByVal iSize As Int32, ByRef lpNumberOfBytesRead As Int32) As Boolean
    Public Declare Function WriteProcessMemory Lib "kernel32" (ByVal hProcess As IntPtr, ByVal lpBaseAddress As IntPtr, ByVal lpBuffer As Byte(), ByVal iSize As Int32, ByRef lpNumberOfBytesRead As Int32) As Boolean
    Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
    Public Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As IntPtr) As Boolean
    Public PROCESS_ALL_ACCESS As UInt32 = &H1F0FFF
    Public Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAcess As UInt32, ByVal bInheritHandle As Boolean, ByVal dwProcessId As Int32) As IntPtr
    Public Declare Sub mouse_event Lib "user32" Alias "mouse_event" (ByVal dwFlags As Int32, ByVal dx As Int32, ByVal dy As Int32, ByVal cButtons As Int32, ByVal dwExtraInfo As Int32)
    Public Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Int32) As Integer

    Public bClient As Integer
    Public bEngine As Integer
    Public menu As New m3mer
    Public csgoHandle As IntPtr
    Public csgoProcess As Process

    Dim thread_noflash As System.Threading.Thread = New System.Threading.Thread(AddressOf NoFlash.NoFlash)
    Dim thread_radar As System.Threading.Thread = New System.Threading.Thread(AddressOf Radar.Radar)
    Dim thread_slowaim As System.Threading.Thread = New System.Threading.Thread(AddressOf SlowAim.SlowAim)
    Dim thread_trigger As System.Threading.Thread = New System.Threading.Thread(AddressOf Trigger.Trigger)
    Dim thread_aimbot As System.Threading.Thread = New System.Threading.Thread(AddressOf Aimbot.Aimbot)
    Dim thread_rainbow As System.Threading.Thread = New System.Threading.Thread(AddressOf Rainbow.Rainbow)
    Dim thread_autopistol As System.Threading.Thread = New System.Threading.Thread(AddressOf AutoPistol.AutoPistol)
    Dim thread_bhop As System.Threading.Thread = New System.Threading.Thread(AddressOf Bhop.Bhop)
    Dim thread_glow As System.Threading.Thread = New System.Threading.Thread(AddressOf Glow.Glow)

    Public Sub Main()
        Try
            csgoProcess = Process.GetProcessesByName("csgo")(0)
            csgoHandle = OpenProcess(PROCESS_ALL_ACCESS, False, csgoProcess.Id)
        Catch ex As Exception
            MsgBox("You don't have CS:GO open? What are you trying to accomplish here?", MsgBoxStyle.Exclamation, "m3mer external")
            Environment.Exit(0)
        End Try

        For Each m0d As System.Diagnostics.ProcessModule In csgoProcess.Modules
            If m0d.ModuleName = "engine.dll" Then
                bEngine = m0d.BaseAddress
            End If

            If m0d.ModuleName = "client.dll" Then
                bClient = m0d.BaseAddress
            End If
        Next

        StartThreads()
        Application.Run(menu)
    End Sub

    Private Sub StartThreads()
        thread_autopistol.Start()
        thread_bhop.Start()
        thread_radar.Start()
        thread_slowaim.Start()
        thread_trigger.Start()
        thread_rainbow.Start()
        thread_aimbot.Start()
        thread_glow.Start()
        thread_noflash.Start()
    End Sub
End Module