﻿Module Offsets
    Public oSpotted As Integer = &H935
    Public oSensitivity As Integer = &HA9EC64
    Public oGlowIndex As Integer = &H86B0
    Public oGlowObject As Integer = &H4B527B4
    Public oFlashAlpha As Integer = &H8694
    Public oBoneMatrix As Integer = &HA78
    Public oViewOffset As Integer = &H104
    Public oVecOrigin As Integer = &H134
    Public oEngine As Integer = &H5D3224
    Public oViewAngles As Integer = &H4CE0
    Public oVecPunch As Integer = &H13E8
    Public oLocalPlayer As Integer = &HA9947C
    Public oEntityList As Integer = &H4A3BA44
    Public oInCross As Integer = &H8CF4
    Public oTeam As Integer = &HF0
    Public oAttack As Integer = &H2EAD958
    Public oJump As Integer = &H4AD0248
    Public oFlags As Integer = &H100
    Public oDormant As Integer = &HE9
    Public oHealth As Integer = &HFC
    Public oVelocity As Integer = &H110
    Public oSpottedByMask As Integer = &H978
End Module