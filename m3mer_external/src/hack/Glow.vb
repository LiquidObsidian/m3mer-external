﻿Module Glow
    Public Structure GlowStruct
        Public r As Single
        Public g As Single
        Public b As Single
        Public a As Single
        Public rwo As Boolean
        Public rwuo As Boolean
    End Structure

    Private Sub DrawGlow(ByVal pGlowIn As Int32, ByVal col As GlowStruct)
        Dim pGlowObj = Mem.ReadInt(bClient + Offsets.oGlowObject, 4)

        Mem.WriteSingle(pGlowObj + ((pGlowIn * &H34) + &H4), 4, col.r)
        Mem.WriteSingle(pGlowObj + ((pGlowIn * &H34) + &H8), 4, col.g)
        Mem.WriteSingle(pGlowObj + ((pGlowIn * &H34) + &HC), 4, col.b)
        Mem.WriteSingle(pGlowObj + ((pGlowIn * &H34) + &H10), 4, col.a)
        Mem.WriteBool(pGlowObj + ((pGlowIn * &H34) + &H24), 1, col.rwo)
        Mem.WriteBool(pGlowObj + ((pGlowIn * &H34) + &H25), 1, col.rwuo)
    End Sub

    Sub Glow()
        While True
            Dim GlowEnemy As GlowStruct = New GlowStruct With {
                .r = Config.glow_enemy_r / 255,
                .g = Config.glow_enemy_g / 255,
                .b = Config.glow_enemy_b / 255,
                .a = Config.glow_alpha / 255,
                .rwo = True,
                .rwuo = False
            }

            Dim GlowTeam As GlowStruct = New GlowStruct With {
                .r = Config.glow_team_r / 255,
                .g = Config.glow_team_g / 255,
                .b = Config.glow_team_b / 255,
                .a = Config.glow_alpha / 255,
                .rwo = True,
                .rwuo = False
            }

            Dim GlowRainbow As GlowStruct = New GlowStruct With {
                .r = RainbowColor(0) / 255,
                .g = RainbowColor(1) / 255,
                .b = RainbowColor(2) / 255,
                .a = Config.glow_alpha / 255,
                .rwo = True,
                .rwuo = False
            }

            If menu.GlowEnemy.Checked Or menu.GlowTeam.Checked Then
                For i As Integer = 1 To 64
                    Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)
                    Dim pCurPlayer = Mem.ReadInt(bClient + Offsets.oEntityList + ((i - 1) * 16), 4)
                    Dim pCurPlayerDormant = Mem.ReadBool(pCurPlayer + Offsets.oDormant, 4)

                    If Not (pCurPlayerDormant) Then
                        Dim pCurPlayerGlowIndex = Mem.ReadInt(pCurPlayer + Offsets.oGlowIndex, 4)
                        Dim pCurPlayerTeam = Mem.ReadInt(pCurPlayer + Offsets.oTeam, 4)
                        Dim pLocalPlayerTeam = Mem.ReadInt(pLocalPlayer + Offsets.oTeam, 4)

                        If (pCurPlayerTeam = pLocalPlayerTeam) Then
                            If menu.GlowTeam.Checked Then
                                If menu.TeamRainbow.Checked Then
                                    DrawGlow(pCurPlayerGlowIndex, GlowRainbow)
                                Else
                                    DrawGlow(pCurPlayerGlowIndex, GlowTeam) ' glow team
                                End If
                            End If
                        Else
                            If menu.EnemyRainbow.Checked Then
                                DrawGlow(pCurPlayerGlowIndex, GlowRainbow)
                            Else
                                DrawGlow(pCurPlayerGlowIndex, GlowEnemy)
                            End If
                        End If
                    End If
                Next
            End If
            Threading.Thread.Sleep(1000 / 60) ' meme 60 fps
        End While
    End Sub
End Module
