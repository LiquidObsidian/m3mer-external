﻿Module Rainbow
    Public RainbowColor() As Integer = {255, 0, 0}
    Private RainbowSpeed As Integer = 1

    Public Sub Rainbow()
        While True
            For i As Integer = 1 To 255
                RainbowColor(2) += 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(0) -= 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(1) += 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(2) -= 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(0) += 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
            For i As Integer = 1 To 255
                RainbowColor(1) -= 1
                Threading.Thread.Sleep(RainbowSpeed)
            Next
        End While
    End Sub
End Module
