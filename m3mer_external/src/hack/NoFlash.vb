﻿Module NoFlash
    Public Sub NoFlash()
        While True
            Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)

            If menu.NoFlash.Checked Then
                If (Mem.ReadSingle(pLocalPlayer + Offsets.oFlashAlpha, 4) > 0.0F) Then
                    Mem.WriteSingle(pLocalPlayer + Offsets.oFlashAlpha, 4, 0.0F)
                End If
            ElseIf (Mem.ReadSingle(pLocalPlayer + Offsets.oFlashAlpha, 4) = 0.0F) Then
                Mem.WriteSingle(pLocalPlayer + Offsets.oFlashAlpha, 4, 255.0F)
            End If

            Threading.Thread.Sleep(100)
        End While
    End Sub
End Module
