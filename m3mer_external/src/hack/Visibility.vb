﻿Module Visibility
    Public Function SpottedByMask(ByVal pPlayer As Integer)
        Dim pLocalIndex As Integer = Mem.ReadInt(bClient + Offsets.oEntityList, 4)
        Dim pMask As Long = Mem.ReadLong(pPlayer + Offsets.oSpottedByMask, 4)
        Return CBool(pMask And (1 + (pLocalIndex - 1)))
    End Function
End Module
