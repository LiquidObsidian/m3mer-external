﻿Module SlowAim
    Public Sub SlowAim()
        While True
            If menu.SlowAim.Checked Then
                Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)
                Dim pInCross = Mem.ReadInt(pLocalPlayer + Offsets.oInCross, 4)

                Mem.WriteSingle(bClient + Offsets.oSensitivity, 4, Config.sensitivity_old)
                If pInCross > 0 And pInCross < 65 Then
                    Dim Team = Mem.ReadInt(pLocalPlayer + Offsets.oTeam, 4)
                    Dim pInCrossPlayer = Mem.ReadInt(bClient + Offsets.oEntityList + ((pInCross - 1) * &H10), 4)
                    Dim InCrossTeam = Mem.ReadInt(pInCrossPlayer + Offsets.oTeam, 4)

                    If Not Team = InCrossTeam Then
                        Mem.WriteSingle(bClient + Offsets.oSensitivity, 4, Config.sensitivity_slowaim)
                    End If
                End If
            End If
            Threading.Thread.Sleep(15)
        End While
    End Sub
End Module