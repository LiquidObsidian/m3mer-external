﻿Module Aimbot
    Public bAimbotting As Boolean = False

    Public Structure Angle
        Public x As Single
        Public y As Single
        Public z As Single
    End Structure

    Public Structure Trace
        Public allsolid As Boolean
        Public startsolid As Boolean
        Public Fraction As Single
        Public endpos As Angle
        'Public UnKnown1[4] As Char
        Public SurfaceFlags As Integer
        Public Contents As Integer
        Public EntityNum As Integer
    End Structure

    Public Function GetBonePosition(ByVal pTargetPlayer As Integer, ByVal pBoneID As Integer)
        Dim pAngle As Angle = New Angle
        Dim BoneMatrix = Mem.ReadInt(pTargetPlayer + Offsets.oBoneMatrix, 4)
        pAngle.x = Mem.ReadSingle(BoneMatrix + &H30 * pBoneID + &HC, 4)
        pAngle.y = Mem.ReadSingle(BoneMatrix + &H30 * pBoneID + &H1C, 4)
        pAngle.z = Mem.ReadSingle(BoneMatrix + &H30 * pBoneID + &H2C, 4)

        Return pAngle
    End Function

    Public Function _NormalizeAngle(ByVal pAngle As Single)
        Return ((pAngle + 180) Mod 360) - 180
    End Function

    Public Function AngleDifference(ByVal pAngleA As Angle, ByVal pAngleB As Angle)
        Dim bX180 As Boolean = False
        Dim bY180 As Boolean = False
        Dim XDiff As Single = _NormalizeAngle(pAngleA.x - pAngleB.x)
        Dim YDiff As Single = _NormalizeAngle(pAngleA.y - pAngleB.y)

        bX180 = 180 > XDiff
        bY180 = 180 > YDiff

        If Not bX180 Then
            XDiff = XDiff - 360
        End If

        If Not bY180 Then
            YDiff = YDiff - 360
        End If

        If 0 > XDiff Then
            XDiff = (XDiff - XDiff) - XDiff
        End If

        If 0 > YDiff Then
            YDiff = (YDiff - YDiff) - YDiff
        End If

        Return XDiff + YDiff
    End Function


    Public Function NormalizeAngle(ByVal pAngle As Angle)
        If pAngle.x > 89 Then
            pAngle.x = 89
        ElseIf -89 > pAngle.x Then
            pAngle.x = -89
        End If

        If pAngle.y > 180 Then
            pAngle.y = pAngle.y - 360
        ElseIf -180 > pAngle.y Then
            pAngle.y = pAngle.y + 360
        End If

        pAngle.z = 0
        Return pAngle
    End Function

    Public Function GetOrigin()
        Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)
        Dim pOrigin As Angle = New Angle
        pOrigin.x = Mem.ReadSingle(pLocalPlayer + Offsets.oVecOrigin, 4)
        pOrigin.y = Mem.ReadSingle(pLocalPlayer + Offsets.oVecOrigin + &H4, 4)
        pOrigin.z = Mem.ReadSingle(pLocalPlayer + Offsets.oVecOrigin + &H4 + &H4, 4)

        Return pOrigin
    End Function

    Public Function GetPunch()
        Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)
        Dim pAngle As Angle = New Angle
        pAngle.x = Mem.ReadSingle(pLocalPlayer + Offsets.oVecPunch, 4)
        pAngle.y = Mem.ReadSingle(pLocalPlayer + Offsets.oVecPunch + &H4, 4)
        pAngle.z = 0

        Return pAngle
    End Function

    Public Function GetAngles()
        Dim pAnglePointer As Integer = Mem.ReadInt(bEngine + Offsets.oEngine, 4)
        Dim pAngle As Angle = New Angle
        pAngle.x = Mem.ReadSingle(pAnglePointer + Offsets.oViewAngles, 4)
        pAngle.y = Mem.ReadSingle(pAnglePointer + Offsets.oViewAngles + &H4, 4)
        pAngle.z = 0

        Return pAngle
    End Function

    Public Sub SetAngles(ByVal pAngle As Angle)
        Dim pAnglePointer As Integer = Mem.ReadInt(bEngine + Offsets.oEngine, 4)
        Mem.WriteSingle(pAnglePointer + Offsets.oViewAngles, 4, pAngle.x)
        Mem.WriteSingle(pAnglePointer + Offsets.oViewAngles + &H4, 4, pAngle.y)
    End Sub

    Public Function mAngle(ByVal x As Single, ByVal y As Single, ByVal z As Single)
        Dim pAngle As Angle = New Angle
        pAngle.x = x
        pAngle.y = y
        pAngle.z = z

        Return pAngle
    End Function

    Public Function GetSmooth(ByVal pOriginal As Angle, ByVal pDest As Angle)
        If Convert.ToSingle(menu.AimSmooth.Text) = 0 Or Convert.ToSingle(menu.AimSmooth.Text) >= 10 Then
            Return pDest
        End If

        Dim pSmooth As Angle = New Angle
        pSmooth.x = pDest.x - pOriginal.x
        pSmooth.y = pDest.y - pOriginal.y
        pSmooth = NormalizeAngle(pSmooth)

        pSmooth.x = pOriginal.x + pSmooth.x / 100 * Convert.ToSingle(menu.AimSmooth.Text)
        pSmooth.y = pOriginal.y + pSmooth.y / 100 * Convert.ToSingle(menu.AimSmooth.Text)
        pSmooth = NormalizeAngle(pSmooth)

        Return pSmooth
    End Function

    Public Function CalcAngle(ByVal pSource As Angle, ByVal pDestination As Angle)
        Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)
        Dim pDelta As Angle = New Angle
        pDelta.x = pSource.x - pDestination.x
        pDelta.y = pSource.y - pDestination.y
        Dim pViewOrigin = Mem.ReadSingle(pLocalPlayer + Offsets.oViewOffset + &H4 + &H4, 4)
        pDelta.z = pSource.z + pViewOrigin - pDestination.z

        Dim siHyp As Single = Math.Sqrt(pDelta.x * pDelta.x + pDelta.y * pDelta.y)

        Dim pPunch As Angle = New Angle
        pPunch = GetPunch()

        Dim pAngle As Angle = New Angle
        pAngle.x = Math.Atan(pDelta.z / siHyp) * 57.295779513082
        pAngle.y = Math.Atan(pDelta.y / pDelta.x) * 57.295779513082
        pAngle.z = 0

        If pDelta.x >= 0.0 Then
            pAngle.y = pAngle.y + 180
        End If

        Return pAngle
    End Function

    Public Sub Aimbot()
        While True
            If GetAsyncKeyState(Keys.LMenu) And menu.Aimbot.Checked = True Then
                Dim pLocalPlayer = Mem.ReadInt(bClient + Offsets.oLocalPlayer, 4)
                Dim pLocalTeam = Mem.ReadInt(pLocalPlayer + Offsets.oTeam, 4)
                Dim pAngles = GetAngles()
                Dim pOrigin = GetOrigin()

                Dim pBone As Integer = 6

                If Not menu.AimbotBone.Text = "" And Not menu.AimbotBone.Text = " " And Not Convert.ToInt32(menu.AimbotBone.Text) = 0 Then
                    pBone = Convert.ToInt32(menu.AimbotBone.Text)
                End If

                For i As Integer = 1 To 64
                    Dim pCurPlayer = Mem.ReadInt(bClient + Offsets.oEntityList + ((i - 1) * 16), 4)

                    If pCurPlayer = 0 Then
                        Continue For
                    End If

                    Dim pCurPlayerTeam = Mem.ReadInt(pCurPlayer + Offsets.oTeam, 4)
                    Dim pCurPlayerDormant = Mem.ReadBool(pCurPlayer + Offsets.oDormant, 4)
                    Dim pCurPlayerHealth = Mem.ReadInt(pCurPlayer + Offsets.oHealth, 4)

                    If Not pLocalTeam = pCurPlayerTeam And Not pCurPlayerDormant And Not 0 >= pCurPlayerHealth Then
                        Dim pAngle = GetBonePosition(pCurPlayer, pBone)
                        pAngle = CalcAngle(pOrigin, pAngle)
                        pAngle = NormalizeAngle(pAngle)

                        If AngleDifference(pAngles, pAngle) > Convert.ToSingle(menu.AimbotFOV.Text) Then 'Or Not Visibility.SpottedByMask(pCurPlayer)
                            Continue For
                        End If

                        bAimbotting = True
                        pAngle.x = pAngle.x - GetPunch().x * 2
                        pAngle.y = pAngle.y - GetPunch().y * 2
                        SetAngles(GetSmooth(pAngles, pAngle))

                        Exit For
                    End If
                Next

                bAimbotting = False
            End If

            Threading.Thread.Sleep(1)
        End While
    End Sub
End Module